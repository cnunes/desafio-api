package cnunes.desafio_rest_assured;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;


public class APITest 
{
	public static String baseURL = "http://localhost:8080/api/v1/";
	
	@Test(description="Consultar CPF sem restrição.")
	public void consultarCPFSemRestricao() {
		String testeCPFSemRestricao = "08506845017";
		
		// cpf sem restrição
		given().pathParam("CPF", testeCPFSemRestricao)
		.when().get(baseURL + "/restricoes/{CPF}")
		.then().statusCode(204);
	}
	
	@Test(description="Consultar CPF com restrição.")
	public void consultarCPFComRestricao() {
		String testCPF = "97093236014";
		
		// cpf com restrição
		given().pathParam("CPF", testCPF)
		.when().get(baseURL + "{CPF}")
		.then().statusCode(200)
		.body("message", equalTo("O CPF " + testCPF + " possui restrição"));
		
	}
	
	@Test(description="Criar simulação com sucesso.")
	public void criarSimulacao() {
		Map<String, String> novaSimulacao = new HashMap<>();
		novaSimulacao.put("cpf", "08506845017");
		novaSimulacao.put("nome", "Fulano da Silva");
		novaSimulacao.put("email", "fulanodasilva@mail.com");
		novaSimulacao.put("valor", "2000");
		novaSimulacao.put("parcela", "20");
		novaSimulacao.put("seguro", "true");
		
		// sucesso
		given().contentType("application/json")
		.formParams(novaSimulacao)
		.when().post(baseURL + "simulacoes")
		.then().statusCode(201)
		.body(contains(novaSimulacao));
		
	}
	
	@Test(description="Criar simulação com erro de regras.")
	public void criarSimulacaoErroRegras() {
		Map<String, String> novaSimulacao = new HashMap<>();
		novaSimulacao.put("cpf", "1");
		novaSimulacao.put("nome", "Fulano da Silva");
		novaSimulacao.put("email", "fulanodasilvamail");
		novaSimulacao.put("valor", "500");
		novaSimulacao.put("parcela", "62");
		novaSimulacao.put("seguro", "neutral");
		
		// erro de regras
		given().contentType("application/json")
		.formParams(novaSimulacao)
		.when().post(baseURL + "simulacoes")
		.then().statusCode(400)
		.body(is(anything()));
	}
	
	@Test(description="Criar simulação com CPF já existente")
	public void criarSimulacaoExistente() {
		Map<String, String> novaSimulacao = new HashMap<>();
		novaSimulacao.put("cpf", "08506845017");
		novaSimulacao.put("nome", "Fulano da Silva");
		novaSimulacao.put("email", "fulanodasilva@mail.com");
		novaSimulacao.put("valor", "2000");
		novaSimulacao.put("parcela", "20");
		novaSimulacao.put("seguro", "true");
		
		// ja existente
		given().contentType("application/json")
		.formParams(novaSimulacao)
		.when().post(baseURL + "simulacoes")
		.then().statusCode(409)
		.body("mensagem", equalTo("CPF já existente"));
	}
	
	@Test(description="Alterar simulação com sucesso.")
	public void alterarSimulacao() {
		String testCPF = "97093236014";
		Map<String, String> novaSimulacao = new HashMap<>();
		novaSimulacao.put("cpf", "08506845017");
		novaSimulacao.put("nome", "Fulano da Silva");
		novaSimulacao.put("email", "fulanodasilva@mail.com");
		novaSimulacao.put("valor", "2000");
		novaSimulacao.put("parcela", "20");
		novaSimulacao.put("seguro", "true");
		
		// sucessso
		given().contentType("application/json")
		.formParams(novaSimulacao)
		.when().put(baseURL + "simulacoes/" + testCPF)
		.then().statusCode(201)
		.body(contains(novaSimulacao));
		
	}
	
	@Test(description="Alterar simulação com erro de regras.")
	public void alterarSimulacaoErroRegras() {
		String testCPF = "08506845017";
		Map<String, String> novaSimulacao = new HashMap<>();
		novaSimulacao.put("cpf", "1");
		novaSimulacao.put("nome", "Fulano da Silva");
		novaSimulacao.put("email", "fulanodasilvamail");
		novaSimulacao.put("valor", "500");
		novaSimulacao.put("parcela", "62");
		novaSimulacao.put("seguro", "neutral");
		
		// erros de regras em cpf com simulacao
		given().contentType("application/json")
		.formParams(novaSimulacao)
		.when().put(baseURL + "simulacoes/" + testCPF)
		.then().statusCode(400)
		.body(is(anything()));
	}
	
	@Test(description="Alterar simulação para CPF já existente")
	public void alterarSimulacaoParaCPFJaExistente() {
		String testCPF = "08506845017";
		Map<String, String> novaSimulacao = new HashMap<>();
		novaSimulacao.put("cpf", "08506845017");
		novaSimulacao.put("nome", "Fulano da Silva");
		novaSimulacao.put("email", "fulanodasilva@mail.com");
		novaSimulacao.put("valor", "2000");
		novaSimulacao.put("parcela", "20");
		novaSimulacao.put("seguro", "true");
		
		// erro ja existente de cpf com simulacao
		given().contentType("application/json")
		.formParams(novaSimulacao)
		.when().put(baseURL + "simulacoes/" + testCPF)
		.then().statusCode(409)
		.body("mensagem", equalTo("CPF já existente"));
		
	}
	
	@Test(description="Alterar simulação com CPF inexistente.")
	public void alterarSimulacaoCPFInexistente() {
		String testCPF = "-1";
		
		Map<String, String> novaSimulacao = new HashMap<>();
		novaSimulacao.put("cpf", "08506845017");
		novaSimulacao.put("nome", "Fulano da Silva");
		novaSimulacao.put("email", "fulanodasilva@mail.com");
		novaSimulacao.put("valor", "2000");
		novaSimulacao.put("parcela", "20");
		novaSimulacao.put("seguro", "true");
		
		// erro de cpf sem simulacao
		given().contentType("application/json")
		.formParams(novaSimulacao)
		.when().put(baseURL + "simulacoes/" + testCPF)
		.then().statusCode(404)
		.body("mensagem", equalTo("CPF não encontrado"));
	}
	
	@Test(description="Lista de simulações quando existir uma ou mais.")
	public void consultarSimulacoes() {
		
		// sucesso
		given()
		.when().get(baseURL + "/simulacoes")
		.then().statusCode(200)
		.body("*", is(anything()));
		
	}
	
	@Test(description="Consultar simulações quando não existir nenhuma simulação cadastrada.")
	public void consultaSemSimulacoes() {
		
		// vazio
		given()
		.when().get(baseURL + "/simulacoes")
		.then().statusCode(204);
		
	}
	
	@Test(description="Consultar CPF com simulação.")
	public void consultarCPFComSimulacao() {
		String cpfConsultado = "08506845017";
		
		// sucesso
		given().pathParam("CPF", cpfConsultado)
		.when().get(baseURL + "/simulacoes/{CPF}")
		.then().statusCode(200)
		.body("*", is(anything()));
		
	}
	
	@Test(description="Consultar CPF sem simulação.")
	public void consultarSimulacaoCPFSemSimulacao() {
		String cpfConsultadoInexistente = "-1";
		
		// cpf sem simulacao
		given().pathParam("CPF", cpfConsultadoInexistente)
		.when().get(baseURL + "/simulacoes/{CPF}")
		.then().statusCode(404);
	}
	
	@Test(description="Remover simulação com sucesso.")
	public void removerSimulacaoSucesso() {
		String idSimulacao = "1";
		
		// sucesso
		given().pathParam("id", idSimulacao)
		.when().get(baseURL + "/simulacoes/{id}")
		.then().statusCode(204);
		
	}
	
	@Test(description="Remover simulação inexistente.")
	public void removerSimulacaoInexistente() {
		String idSimulacaoInexistente = "-1";
		
		// id da simulação não existe
		given().pathParam("id", idSimulacaoInexistente)
		.when().get(baseURL + "/simulacoes/{id}")
		.then().statusCode(404)
		.body("mensagem", equalTo("Simulação não encontrada"));
		
	}
   
}
